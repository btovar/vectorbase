<div class="<?php print $classes;?> clearfix">

    <?php if (isset($title_suffix['contextual_links'])): ?>
        <?php print render($title_suffix['contextual_links']); ?>
    <?php endif; ?>
    <?php if ($descript): ?>
        <div class="ds-descript<?php print $descript_classes; ?>">
            <?php print $descript; ?>
        </div>
    <?php endif; ?>
    <?php if ($descript2): ?>
        <div class="ds-descript2<?php print $descript2_classes; ?>">
            <?php print $descript2; ?>
        </div>
    <?php endif; ?>
    
    <?php if ($infoButton): ?>
        <div class="ds-infoButton<?php print $infoButton_classes; ?>">
            <?php print $infoButton; ?>
        </div>
    <?php endif; ?>


    <?php if ($rowFluid): ?>
        <div id="rowFluid" class="ds-rowFluid <?php print $rowFluid_classes; ?>">
            <?php print $rowFluid; ?>
        </div>
    <?php endif; ?>

    <?php
    drupal_add_library('system', 'ui.accordion');
    drupal_add_library('system', 'ui.button');
    drupal_add_css(drupal_get_path('module', 'user_suite'). '/css/font-awesome/css/font-awesome.css', 'file');
    drupal_add_css(drupal_get_path('module', 'user_suite'). '/bootstrap/css/bootstrap.css', 'file');
    drupal_add_css(drupal_get_path('module', 'user_suite'). '/bootstrap/css/bootstrap-responsive.css', 'file');
    drupal_add_css(drupal_get_path('module', 'user_suite'). '/bootstrap/css/bootstrap-switch.css', 'file');
    drupal_add_css(drupal_get_path('module', 'user_suite'). '/css/profile.css', 'file');
    drupal_add_js(drupal_get_path('module', 'user_suite'). '/bootstrap/js/bootstrap.js', 'file');
    drupal_add_js(drupal_get_path('theme', 'acquia_marina'). '/js/download_node.js', 'file');
    ?>

</div>
