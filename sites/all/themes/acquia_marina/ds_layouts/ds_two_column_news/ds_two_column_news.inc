
<?php
function ds_ds_two_column_news() {
    return array(
        'label' => t('Two Column News'),
        'regions' => array(
            'left' => t('Left'),
	    'right' => t('Right')
            ),
        // Add this line if there is a default css file.
        'css' => TRUE,
    );
}
?>
