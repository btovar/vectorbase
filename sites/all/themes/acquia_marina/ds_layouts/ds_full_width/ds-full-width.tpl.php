<div class="<?php print $classes;?> clearfix">

    <?php if (isset($title_suffix['contextual_links'])): ?>
        <?php print render($title_suffix['contextual_links']); ?>
    <?php endif; ?>
    <?php if ($Field): ?>
        <div class="ds-field<?php print $field_classes; ?>">
            <?php print $Field; ?>
        </div>
    <?php endif; ?>

    <?php
    drupal_add_css(drupal_get_path('theme', 'acquia_marina'). '/ds_layouts/ds_full_width/ds_full_width', 'file');
    ?>

</div>
