<?php
/**
 * This code does the work to save/delete job results from
 * users. Only two parameters are passed from the client:
 * Job ID and a boolean to save that job's results 
 * (true = save, false = delete). Before any peristant
 * actions are exectuted, the database is checked first,
 * and then the filesystem. If both are in states that
 * agree with the desired operation, the database will be
 * updated first, and then the file itself. If the database
 * operation fails for some reason, the entire action is
 * aborted. If the file operation fails, the error is
 * logged even though the database was updated. It is
 * up to the admin/admin scripts to resolve these failures
 * if they occur. */

require_once('constants.php');

/** Drupal server root used to bootstrap the D7 environment by scripts
 * that need to use the D7 APIs. */
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Get and validate the POST variables
$jobId = isset($_POST['jobId']) ? $_POST['jobId'] : null;
$realId = isset($_POST['realId']) ? $_POST['realId'] : null;
$op = isset($_POST['op']) ? $_POST['op'] : null;
$opVal = isset($_POST['opVal']) ? $_POST['opVal'] : null;
$user = isset($_POST['uid']) ? $_POST['uid'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

if(is_null($jobId) || is_null($op) || !welcome_user_is_logged_in($user, $sid)) {
	echo USER_JOB_PARAMETERS_NOT_FOUND . ", Job Id: $jobId, Op: $op, User: $user, Sid: $sid";
	return;
}

function welcome_user_is_logged_in($usr, $sesId) {
	if(is_null($usr) || is_null($sesId)) {
		return false;
	}
	$c = pg_connect("host=localhost dbname=vb_drupal user=db_public password=limecat"); 
	$qry = 'SELECT count(*) FROM sessions WHERE uid = $1 AND sid = $2';
	pg_prepare($c, 'getSession', $qry);
	$res = pg_execute($c, 'getSession', array($usr, $sesId));
	$data = pg_fetch_assoc($res);
	pg_close($c);
	return $data['count'] > 0;
}

// Create the proper path to the files.
$savedFile = "/sites/default/files/ftp/" . USER_JOB_RESULTS_SAVE_DIRECTORY . "/$jobId.txt";

// Create a reusable database connection for all of our DB interactions in this script.
$conn = pg_connect("host=localhost dbname=vb_drupal user=db_public password=limecat"); 

// See if we've saved these results in the past (relevant to both saves and deletes).

// Tells if this job already has a saved entry in the db.
$alreadySaved = false;

// Tells if we need to import the file from the grid-system's results
$importFile = false;

$q = 'SELECT count(x.job_id) FROM xgrid_job_params x WHERE x.job_id = $1 AND x.argument = \'' . USER_JOB_XGRID_PATH_ARGUMENT . '\'';
pg_prepare($conn, 'findSaved', $q);
$r = pg_execute($conn, 'findSaved', array($jobId));
if($r !== false) {
	$c = pg_fetch_assoc($r);
	$alreadySaved = $c['count'] > 0;
}

// Check if the file exists
if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $savedFile)) {
	$importFile = true;
}


if($op === 'save') {

	if(is_null($realId)) {
		echo USER_JOB_PARAMETERS_NOT_FOUND . ': Missing results grid id.';
		return;
	}

	if($importFile) {
		/*$client = new SoapClient(XGRID_URL, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 5, 'soap-version'=>SOAP_1_1));
		
		$statusOutput = $client->getStatus($realId);
		$match = array();	
		preg_match("#jobStatus = (.*?);#",$statusOutput,$match);
		$status = '';
		if (!empty($match)) {
			$status = $match[1];
		}*/
		$status = tool_helpers_status($jobId);
		if ($status!='Completed') {
			echo USER_JOB_WRONG_STATUS . ": This job either does not exist on the grid server or it did not finish correctly. Status: \"'$status'\"";
			return;
		}
		$theDir = dirname($_SERVER['DOCUMENT_ROOT'] . $savedFile);
		if(!is_dir($theDir)) {
			$madeDir = mkdir($theDir);
			if(!$madeDir) { 
				echo USER_JOB_MKDIR_FAILED . ": Could not create a directory to save the results.";
				return;
			}
		}
    $saveToHere = $_SERVER['DOCUMENT_ROOT'] . $savedFile;
    //Might need to change tool_helpers_results_content to tool_helpers_results
    //in the future
    $res = tool_helpers_results_content($jobId);
    if(file_put_contents($saveToHere, $res) === false) {
			echo USER_JOB_SAVE_FAILED . ': Could not save results to file (possibly a permissions issue).';
			return;
		}
	}
	if(!$alreadySaved) {
		$addSaved = 'INSERT INTO xgrid_job_params values ($1, \'' . USER_JOB_XGRID_PATH_ARGUMENT . '\', $2)';
		pg_prepare($conn, 'addSaved', $addSaved);
		if(pg_execute($conn, 'addSaved', array($jobId, $savedFile)) === false) {
			echo USER_JOB_SAVE_SQL_FAILED . ': ' . pg_last_error($conn);
			return;
		}
	}
	echo USER_JOB_OP_EXE_SUCCESS . ":$savedFile";
	return;

} else if($op === 'delete') {
	if($alreadySaved) {
		$removeSaved = "DELETE FROM xgrid_job_params where argument = '" . USER_JOB_XGRID_PATH_ARGUMENT . '\' AND job_id = $1';
		pg_prepare($conn, 'removeSaved', $removeSaved);
		if(pg_execute($conn, 'removeSaved', array($jobId)) === false) {
			echo USER_JOB_DELETE_SQL_FAILED . ': Could not remove "saved url" entry from the database.';
			return;
		}
	}
	if(!$importFile && !unlink($_SERVER['DOCUMENT_ROOT'] . $savedFile)){
		echo USER_JOB_DELETE_FAILED . ': Could not remove results file from the filesystem.';
		return;
	}
} else if($op === 'edit-description') {

	$opVal = trim($opVal);
	// If we have no value for this operation (add/edit the description), then error
	if(empty($opVal) || strcasecmp($opVal, 'No description available') === 0) {
		echo USER_JOB_PARAMETERS_NOT_FOUND . ', description is empty.';
		return;
	}

	// Now let's see if there's already a description for this job in the database.
	$descDb = '';
	$q = 'SELECT value FROM xgrid_job_params x WHERE x.job_id = $1 AND x.argument = \'' . USER_JOB_XGRID_DESCRIPTION_ARGUMENT . '\'';
	pg_prepare($conn, 'findDescription', $q);
	$r = pg_execute($conn, 'findDescription', array($jobId));
	if($r !== false) {
		// If there is a result, save it.
		if(pg_num_rows($r) > 0) {
			$c = pg_fetch_assoc($r);
			$descDb = $c['value'];
		} else { // otherwise, null this value out.
			$descDb = null;
		}
	} else {
		echo USER_JOB_SAVE_FAILED . ': Couldn\'t exectue query to see if a description pre-exists.';
		return;
	}

	// No description pre-exists, so insert a new one.
	if(is_null($descDb)) {
		$q = 'INSERT INTO xgrid_job_params values($1, \'' . USER_JOB_XGRID_DESCRIPTION_ARGUMENT  . '\', $2)';
		pg_prepare($conn, 'insertDescription', $q);
		$r = pg_execute($conn, 'insertDescription', array($jobId, $opVal));
		if($r === false) {
			echo USER_JOB_SAVE_SQL_FAILED . ': Saving new description failed.';
			return;	
		}
	} else if(strcasecmp($descDb, $opVal) !== 0) { // Only update the description if the new is different from the old.
		$q = 'UPDATE xgrid_job_params SET value = $1 WHERE job_id = $2 AND argument = \'' . USER_JOB_XGRID_DESCRIPTION_ARGUMENT . '\'';
		pg_prepare($conn, 'updateDescription', $q);
		$r = pg_execute($conn, 'updateDescription', array($opVal, $jobId));
		if($r === false) {
			echo USER_JOB_SAVE_SQL_FAILED . ': Updating description in database failed.';
			return;	
		}
	} else {
		echo USER_JOB_DESC_IS_SAME . ': This description is already saved in the database.';
		return;
	}
}

echo USER_JOB_OP_EXE_SUCCESS;
return;

