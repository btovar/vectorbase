(function ($) {

$(document).ready(function() {
    // prob was argument to the function is an associative array?!?
    // var associativeArray = {}; // associativeArray["one"] = "First";                   
    // var data = "manual annotation, alignment, aaaa, bbbb, ccccc, dddddd".split(",");
    jQuery15("#species-filler").autocomplete({source:data}); 
    jQuery15("#edit-desc").autocomplete({source:data});
    jQuery15("#suggest").autocomplete({source:data});    
    jQuery15('#tabs').tabs();
    jQuery15('#sortable').sortable({items: 'li'});
    jQuery15('.accordion').accordion({
        autoHeight: false,
        clearStyle: true,
        header: 'h3'
    });
});

})(jQuery);
