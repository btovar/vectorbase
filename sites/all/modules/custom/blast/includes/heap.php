<?php
/**
 * MaxHeap object tayolored to comparing (ranking) user job counts.
 * 
 * @package VectorBase
 * @filesource
 */

/**
 * Class used to store the data used by the JobCountHeap class.
 *
 * @package VectorBase
 */
class JobCount {
	private $usr;
	private $jobs;

	function __construct($usr, $jobs = null) {
		$this->usr = $usr;
		$this->jobs = $jobs;
	}

	public function addJob($job, $freq) {
		if(!$this->jobs) {
			$this->jobs = array();
		}
		$this->jobs[$job] = $freq;
	}

	public function getUsr() {
		return $this->usr;
	}

	public function getJobs() {
		return $this->jobs;
	}
	public function getTotalJobCount() {
		$ct = 0;
		foreach($this->jobs as $freq) {
			$ct+=$freq;	
		}
		return $ct;
	}
}

/**
 * This extension of the SplHeap overrides the compare function to
 * allow us to "rank" our blast jobs.  The ranking
 * is used to show the "top X" users of our tools.
 *
 * @package DataFiles
 */
class JobCountHeap extends SplHeap
{

	public function compare($value1, $value2) {

		$f1 = $value1->getTotalJobCount();
		$f2 = $value2->getTotalJobCount();

		if($f1 > $f2) {
			return 1;
		} else if ($f1 < $f2) {
			return -1;
		}
		return 0;
	}

}


