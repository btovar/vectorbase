<?php

define('BLAST_SCAN_BUTTON_TEXT', 'Scan for new databases');
define('BLAST_TOP_JOB_USERS_TEXT', 'View Top 10 users for blast (and other tool) submit jobs');
define('BLAST_SMART_DETECTION_TEXT', 'Enable sequence validation (experimental)');
define('BLAST_SMART_DETECTION_VAR', 'blast_smart_detection_enabled');

