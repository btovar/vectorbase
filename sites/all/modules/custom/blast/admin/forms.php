<?php
/**
* This file contains all the support functions realted to rendering pages,
* generating html, etc that are part of the blast module. 
* 
* @package blast
* @filesource
*/

require_once(DRUPAL_ROOT . '/' . drupal_get_path('module', 'blast') . '/includes/constants.php'); 
require_once(DRUPAL_ROOT . '/' . drupal_get_path('module', 'blast') . '/includes/heap.php');
/**
* Function used to construct the Administration page for this
* module.
* 
* @return string[] Render array defining the form.
*/
function blast_admin_form() {

   $form['status_area'] = array(
            '#title' => 'Status',
            '#prefix' => '<div id="status-div">',
            '#suffix' => '</div>',
            '#type' => 'fieldset',
            '#description' => 'Blast status area.',

   );

   $form['scan_dbs'] = array(
            '#type' => 'button',
            '#value' => t(BLAST_SCAN_BUTTON_TEXT),
            '#ajax' => array(
                     'callback' => 'blast_admin_ajax_handler',
                     'method' => 'replace',
                     'effect' => 'fade',
                     'progress' => array('type' => 'throbber', 'message' => t('Scanning')),
                     'wrapper' => 'status-div',
            )
   );
   $form['view_top_users'] = array(                                                                                              
            '#type' => 'button',
            '#value' => t(BLAST_TOP_JOB_USERS_TEXT),
            '#ajax' => array(
                     'callback' => 'blast_admin_ajax_handler',
                     'method' => 'replace',
                     'effect' => 'fade',
                     'progress' => array('type' => 'throbber', 'message' => t('Compiling')),
                     'wrapper' => 'status-div',
            )   
   );  

    $form['smart_detection'] = array(
        '#type' => 'checkbox',
        '#title' => t(BLAST_SMART_DETECTION_TEXT),
	'#default_value' => variable_get(BLAST_SMART_DETECTION_VAR, false),
	'#description' => t('<article>This feature is in the experimental phase and attempts to alleviate the time and effort of the biologist and the VectorBase servers by validating the query input before it gets sent to the job server. This feature first checks for one of three formats using some regexp: fasta, genebank, and "bare sequences". Once it has collected what it thinks is the set of individual queries (with headers and white space stripped), it then uses the follwing classification algorithm: <i>(minimum threshold offset of 0.8) + (probability of a DNA character to be found in the amino acid alphabet 0.16)^(length of the query string / 16)</i> This gives a tolerance for how "dirty" a query string can be based on length (shorter sequences must be cleaner). However, it also levels off at a threshold of 80% after a certian point. The "/16" segment is to make the leveling off happen "later" (longer sequences, like in the 1000s instead of at 50) than it would with just the length by itself. The algorithm logically checks for DNA first since that alphabet is a subset of the amino acid alphabet. If it determins it is not DNA, it then expands it\'s alphabet and  checks to see if it is made of amino acids. Without further extensive testing and analysis, however, it cannot be guaranteed that this method will work in every scenario. That is why the feature can be disabled. If it is disabled, erroneous input will simply give back a job with blank results or an error from the job server.</article>'),
        '#ajax' => array(
                     'callback' => 'blast_admin_ajax_handler',
                     'method' => 'replace',
                     'effect' => 'fade',
                     'progress' => array('type' => 'throbber', 'message' => t('Saving')),
                     'wrapper' => 'status-div',
)
    );

   return $form;
}


/** 
* Worker function called by all "button-driven" actions from the Administration
* page.
* 
* At the end of each action, they return (or should return) feedback via the 
* $form['status_area']['#description'] field.
*
* @param string[] $form Array defining the structure of the Admin page. This
* variable contains the status area which gets updated by each action.
* @param string[] $form_state Contains state information about the form's
* components, like which button is currently clicked.
*
* @return string[] Status area component in the Admin  page
*/
function blast_admin_ajax_handler($form, $form_state) {

	if($form_state['triggering_element']['#title'] === BLAST_SMART_DETECTION_TEXT) {
		$whatHappened = $form['smart_detection']['#value']; 
		variable_set(BLAST_SMART_DETECTION_VAR, $whatHappened);
		$form['status_area']['#description'] = 'Smart detection has been ' . ($whatHappened ? 'enabled.' : 'disabled.');
	}

	switch($form_state['clicked_button']['#value']) {
		case t(BLAST_SCAN_BUTTON_TEXT):
			$errors = array();
			array_push($errors, "You just pressed a button. Congrats!\n");
			$form['status_area']['#description'] = implode(',', $errors);
			break;
		case t(BLAST_TOP_JOB_USERS_TEXT):
			$q = db_select('xgrid_job_params', 'u');
			$q->addField('u', 'value', 'user');
			$q->addField('p', 'value', 'program');
			$q->addExpression('COUNT(*)', 'frequency'); 	
			$q->innerJoin('xgrid_job_params', 'p', 'u.job_id = p.job_id');
			$q->condition('u.argument', 'user_name', '=');
			$q->condition('p.argument', 'program', '=');
			$q->groupBy('u.value')->groupBy('p.value');
			$q->orderBy('user')->orderBy('program')->orderBy('frequency', 'DESC');
			$results = $q->execute();
			$preSort = array();
			foreach($results as $res) {
				if(!array_key_exists($res->user, $preSort)) {
					$preSort[$res->user] = new JobCount($res->user);
				}
				$preSort[$res->user]->addJob($res->program, $res->frequency);
			}
			$topUsers = new JobCountHeap();	
			foreach($preSort as $pre) {
				$topUsers->insert($pre);
			}
			
			if(!$topUsers->isEmpty()) {
				$topUsers->top();
				$summary = '<ol><lh>Top 10 Users and Number of Jobs They have Submitted</lh>';
				$top10Counter = 0;
				while($topUsers->valid() && $top10Counter < 10) {
					$cur = $topUsers->current();
					$summary .= '<li>';
					if($user = user_load_by_name($cur->getUsr())) {
						$summary .= "{$user->name} (id = {$user->uid})";
					} else {	
						$summary .= 'Anonymous';
					}
					$summary .= ': ';
					$summary .= $cur->getTotalJobCount();
					$summary .= '</li>';
					$topUsers->next();
					$top10Counter++;
				}
				$summary .= '</ol>';
				$form['status_area']['#description'] = $summary;
			} else {
				$form['status_area']['#description'] = "Could not rank users";
			}
			break; 
	}

	return $form['status_area'];
}

