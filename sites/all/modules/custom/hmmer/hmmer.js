(function ($) {

    // making this function a jquery plugin so that it can be called within the module form ajax callback
    $.fn.scrollToElement = function(data) {
        $("html, body").animate({scrollTop: $('#' + data).offset().top-20}, "slow");
    };

    $(document).ready(function () {

        // This is a drupal function found in misc/ajax.js that we are overriding to load the popup dialog prior to submitting the AJAX form submit.
        Drupal.ajax.prototype.beforeSubmit = function(xmlhttprequest, options) {
            // Replacement code. Make sure this is the hmmer form just to be safe.
            // this property is set by the drupal form api in the .module file.
            if( this.hmmerSubmit ) {
                submitHMMer();
            }
            if (this.deleteDatasetsHmmer) {
                alert("Datasets Deleted!\n(refresh the page to reflect the change)");
            }
            if (this.saveDatasetsHmmer) {
                alert("Datasets Saved!");
            }
            if (this.resetPageHmmer) {
                window.location.reload();
            }
        }

        // when the user presses the 'Select All Datasets' checkbox, check all of the other datasets
        $('#edit-select-all-datasets').live("click",function (event) {
           if($(this).is(":checked")) {
               $('.pepDbs.dbs').each( function () {
                   $(this).attr('checked','checked');
               })
               if($('#edit-custom-db-file').val() != ""){
                   $('#edit-custom-checkbox').attr('checked', 'checked');
               }           }
           // uncheck all of the datasets in this case
           else {
               $('.pepDbs.dbs').each( function () {
                   $(this).attr('checked','');
               })
               if($('#edit-custom-db-file').val() != ""){
                   $('#edit-custom-checkbox').attr('checked', '');
               }
           }

        });

        // if any of the individual datasets are clicked, uncheck the 'select all' checkbox since the user has overridden
        // the mass selection.
        $('.pepDbs.dbs').live("click", function(event) {
            $('#edit-select-all-datasets').attr('checked', '');
        })

        $('#datasetSelect').live("change",function(event) {
           $("html, body").scrollTop($($(this).val()).offset().top-60);
        });

		// set default values depending on which kind of cut-off is selected
		$("#edit-cutoff").live("click",function (event) {
				//alert($('input:radio[name="cutoff"]:checked').val());

            if(Drupal.settings.datasetExistsHmmer == false){
                if( $('input:radio[name="cutoff"]:checked').val()=='evalue'){
                    //evale selected
                    $('#edit-sigsequence').val("0.01");
                    $('#edit-sighit').val("0.03");
                    $('#edit-reportsequence').val("1");
                    $('#edit-reporthit').val("1");
                }else{
                    //bit score selected
                    $('#edit-sigsequence').val("25");
                    $('#edit-sighit').val("22");
                    $('#edit-reportsequence').val("7");
                    $('#edit-reporthit').val("5");
                }
            }
		});

    /**********************************************
     job has been submitted. we have an id returned.
     make pop up and wait until we have some job results
     */



    var statusRepeat;
    var jobId;
    var isRaw=true;

    var pinwheel='<br/><img src="'+Drupal.settings.hmmer.hmmerPath+'/ajax-loader.gif">';

    function submitHMMer() {

        // create dialog popup
        $("#submissionDialog").dialog({
            autoOpen: true,
            show: "scale",
            hide: "scale",
            width: 300,
            height: 200,
            draggable: false,
            modal: true,
            title:"HMMer Job Status"
        });

        if($('#edit-lookup').val()==''){
            $("#submissionDialog").html('Submitting job'+pinwheel);
        }else{
            $("#submissionDialog").html('Looking up job'+pinwheel);
        }
    }

    // hidden job id element has changed and the new value is presumably a new job id
    $('#edit-jobid').bind('DOMNodeInserted DOMNodeRemoved', function(event) {
        if (event.type == 'DOMNodeInserted' && $('#edit-jobid').text()!='') {
            parseId=$('#parseJobId').text();
            jobId=parseId;

            $("#submissionDialog").dialog("open");
            $("#submissionDialog").html('Job '+parseId+' is running'+pinwheel);
            // keep checking status until we're all done
	    getJobStatus()
        }
    });	//end edit-jobid has changed


function getJobStatus() {
 
         $.ajax({
                 type: "GET",
                 url: "/tool_helpers/rest/" + jobId + "/wait",
                 timeout: 3200000,
                 success: function(status) {
                         $.ajax({
                                     type: "POST",
                                     //url: Drupal.settings.hmmer.hmmerPath + "/displayResults.php",
                                     url: "/tool_helpers/rest/" + jobId + "/hmmer_results",
                                     data: "id=" + jobId + "&ieIsCrap=" + Math.round(new Date().getTime() / 1000.0),
                                     success: function(msg) {
                                             $("#edit-result").html(msg);
                                             $("#submissionDialog").dialog("close");
                                             $().scrollToElement("edit-result"); //scroll to the results element if the job retriev    al was successful.
                                             $("#edit-jobid").html('');
                                     },
                                     error: function(msg) {
                                                    $("#submissionDialog").dialog("open");
                                                    $("#submissionDialog").html('Job ' + jobId + ' encountered an error while parsi    ng results: ' + msg.responseText);
                                            }
                             });
 
 
                 },
                 error: function(msg) {
                        $("#submissionDialog").dialog("open");
                        $("#submissionDialog").html("Error: " + msg.responseText);
                }
         });
}  
	/*
     end of job submission handling
    **********************************************/

    //Function to clear the datasets so that only the datasets of the job being looked up are checked
    $.fn.clearDatasets = function() {
        jQuery('input:checkbox').removeAttr('checked');
    };

    });

})(jQuery);
