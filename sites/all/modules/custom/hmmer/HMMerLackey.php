<?php

use VectorBase\ToolHelpers\JobSpec;
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');
//require_once(drupal_get_path('module', 'tool_helpers'). '/Lackey.php');

class HMMerLackey extends VectorBase\ToolHelpers\Lackey {
    protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
       
	if (user_is_logged_in()) {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/users";
	}
	else {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/anonymous";
	}
	
	$path_to_submit_files = DRUPAL_ROOT . "/data/job_submit"; 
        # write single query file
        file_put_contents("$path_to_result_files/$prefix.query.fa",$job_specification->get_input());
        
        # create individual submit files
        $submitFile_str = "#!/bin/sh\n\n" .
			"source ~condor/software/condor.sh\n\n";

	$job_attribs = $job_specification->get_attributes();
	$target_dbs =  $job_specification->get_target_dbs();
	
	//The counter is used to give unique names to the .hmm files used by hmmersearch
	$counter = 1;

        foreach ($target_dbs as $db) {
            $submit_str =   "universe        = vanilla\n" .
                            "executable      = /vectorbase/scripts/hmmer.sh\n" .
                            "output          = $path_to_result_files/$prefix.results.$db.out\n" .
                            "log             = $path_to_result_files/$prefix.results.$db.log\n" .
                            "error           = $path_to_result_files/$prefix.results.$db.err\n" .
                            "\n";

        if( preg_match('/^custom_user_db_/', $db)) {
            $db_filename = substr($db,15); // cut out the prepended 'custom_user_db_' string
            $dbFullPath = drupal_realpath("public://userHMMer/$db_filename"); // uri to path conversion

            // effectively a 'move file to job_results folder and add the job id before the filename'
            $db_contents = file_get_contents($dbFullPath);
            file_put_contents("$path_to_result_files/$prefix.$db_filename", $db_contents);

            // after moving the file to the job_results folder need to delete it from the drupal db/filesystem
            $files = file_load_multiple(array(), array('uri' => "public://userHMMer/$db_filename"));
            $file = reset($files);
            file_delete($file);

            // assign the actual db_file path to add to the condor submit script
            $db_file = "$path_to_result_files/$prefix.$db_filename";
         }
         else {
                $db_file =  "/vectorbase/dbs/$db";
         }

            $submit_str .=  "Arguments = " . $job_specification->get_program() . " " . $prefix . "_" . $counter . " $db_file $prefix.query.fa ";
            
            foreach ($job_attribs as $attrib => $value) {
                $submit_str .= "$attrib $value ";
            }
            
            $submit_str .=  "\n\n" .
           		    "Initialdir		 = $path_to_result_files\n" . 
			    "Transfer_executable = false\n" .
			    "Queue\n";

            $submit_filename = "$path_to_submit_files/$prefix" . "_$db.condor";
            file_put_contents($submit_filename,$submit_str);
            
            # add to the dag file string
            $submitFile_str .= "condor_submit $submit_filename\n";

            $counter += 1;
        }

	#Create a condor submission script that will combine all of the output of prevous condor scripts
	$submit_str = "universe		= vanilla\n" .
		      "executable	= /vectorbase/scripts/combineHMMerResults.sh\n" .
		      "Output		= $prefix.parse.out\n" .
		      "Error		= $prefix.parse.err\n" .
		      "Log		= $prefix.parse.log\n" .
		      "\n" .
		      "Arguments	= $prefix $path_to_result_files $prefix.results.*.out " .  
		      "$prefix.results.*.err " . count($target_dbs) . "\n\n" .
		      "Initialdir	= $path_to_result_files\n" .
		      "Transfer_executable = false\n" .
		      "Queue\n";
 
	$submit_filename = "$path_to_submit_files/$prefix.parse.condor";
	file_put_contents($submit_filename, $submit_str);
	
	$submitFile_str .= "condor_submit $submit_filename\n";

        $submitFile_path = "$path_to_submit_files/$prefix.dag";
        file_put_contents($submitFile_path,$submitFile_str);
        
        return array("bash", $submitFile_path);
    }

	public function getRunTime() {
		return 0;
	}
}

