<?php

/**
 * @file
 * VectorBase's hmmer module
 *
*/

use Vectorbase\ToolHelpers\JobSpec;
include_once('HMMerLackey.php');

function hmmer_block_view($delta=''){
  switch($delta) {
    case 'hmmer_block':
      drupal_add_library('system', 'ui.dialog');
      drupal_add_library('system', 'effects.bounce');
      drupal_add_library('system', 'effects.scale');

      // draw block
      $blocks['subject'] = t('Biosequence analysis using profile hidden Markov models');
      $blocks['content'] = drupal_get_form('ajax_hmmer_genform');
      return $blocks;
      break;
  }
}

function hmmer_getClustal($post){
	$id=$post['id'];
	if($id){
		return clustalw_getAlignOut($id);
	}
}

function ajax_hmmer_genform($form, &$form_state) {
    global $user;
    if($user -> uid) $dFault = variable_get(SAVE_DATASETS_VAR_HMMER.$user->uid);
    if(!$dFault){
        $dFault['type'] = 'PROTEIN';
        $dFault['program'] = 'hmmsearch';
        $dFault['cutoff'] = 'evalue';
        $dFault['sigSequence'] = '0.01';
        $dFault['sigHit'] = '0.03';
        $dFault['reportSequence'] = '0.01';
        $dFault['reportHit'] = '0.03';
        drupal_add_js(array('datasetExistsHmmer' => FALSE), 'setting');
    }
    else drupal_add_js(array('datasetExistsHmmer' => TRUE), 'setting');

  $form['hmmer'] = array(
    '#type' => 'hmmer'
  );

    $form['clustalw']['#attached']['css'] = array(
        drupal_get_path('module', 'hmmer').'/hmmer.css'
    );

    $form['clustalw']['#attached']['js'] = array(
        drupal_get_path('module', 'hmmer').'/hmmer.js'
    );

    $form['hmmer']['jobid'] = array(
        '#type' => 'item',
        '#prefix' => '<div id="submissionDialog"></div>',

        '#attributes' => array(
            'style' => array('display' => 'none'),
        ),
    );
	  $form['hmmer']['seqControl']['sequence'] = array(
	    '#type' => 'textarea',
	    '#resizable' => FALSE,
        '#rows' => 15, // make sure there are enough rows to fill in the height value from the css
	    '#cols' => 50,
		'#default_value' => t(hmmer_getClustal($_POST)),
            '#prefix' => '<div id="warning"><div></div></div>
			  <div id="instructions"><div></div></div><div>', // these nested divs are here for an outer div to have 100% width so the inner div can have margins applied appropriately. All divs are needed
	    '#attributes' => array('placeholder' => 'Paste ClustalW alignment for hmmsearch, FASTA for phmmer')
	  );

	/* CONTROL FIELD */

	  $form['hmmer']['seqControl']['controlField'] = array(
		'#type' => 'fieldset',
		'#title' => t('Job Control'),
		'#suffix' => '</div>',  // end div for sequence and control
	  );
	
	  $form['hmmer']['seqControl']['controlField']['lookup'] = array(
		'#type' => 'textfield',
		'#title' => t('Load results'),
		'#attributes' => array('placeholder' => 'Job ID'),
		'#size' => 12, 
		'#maxlength' => 12,
        '#ajax' => array(
              'callback' => 'ajax_hmmer_handler',
              'wrapper' => 'test',
              'effect' => 'fade',
              'progress' => array('type' => 'none'),
              'event' => 'none',// setting the event to none keeps a blur from submitting the form
              'hmmerSubmit' => TRUE,//set this property so the javascript beforeSubmit function gets executed
           'keypress' => TRUE )// setting the keypress to true makes the ENTER key press submit the form
	  );

	   $form['hmmer']['seqControl']['controlField']['description'] = array(
		'#type' => 'textfield',
		'#title' => t('Add Description'),
		'#attributes' => array('placeholder' => 'Description'),
		'#size' => 12,
		'#maxlength' => 50,
	  ); 



	  $form['hmmer']['seqControl']['controlField']['submit'] = array(
	    '#id' => 'hmmerSubmitButton',
	    '#type' => 'submit',
	    '#value' => t('Submit'),
	    '#ajax' => array(
	      'callback' => 'ajax_hmmer_handler',
	      'wrapper' => 'test',
	      'effect' => 'fade',
          'progress' => array('type' => 'none'),
          'hmmerSubmit' => TRUE)//set this property so the javascript beforeSubmit function gets executed
	  );

    $form['hmmer']['seqControl']['controlField']['clear'] = array(
        //   '#name' => 'clear',
        '#type' => 'button',
        '#value' => t('Reset'),
        '#id' => 'clearButton',
        '#ajax' => array(
            'callback' => 'this.form.reset(); document.getElementById(\'edit-sequence\').value = ""; document.getElementById(\'edit-result\').innerHTML=\'\';return false;',
            'resetPageHmmer' => TRUE),
    );

    if($user -> uid){
        $form['hmmer']['deleteDatasets'] = array(
            '#id' => 'deleteButton',
            '#type' => 'button',
            '#value' => t('Delete Saved Options'),
            '#ajax' => array(
                'deleteDatasetsHmmer' => TRUE,
                'callback' => 'delete_datasets_hmmer',
                'wrapper' => 'test',
                'effect' => 'fade',
                'progress' => array('type' => 'none'),
            )
        );
        $form['hmmer']['saveDatasets'] = array(
            '#id' => 'saveButton',
            '#type' => 'button',
            '#value' => t('Save Set Options'),
            '#ajax' => array(
                'saveDatasetsHmmer' => TRUE,
                'callback' => 'save_datasets_hmmer',
                'wrapper' => 'test',
                'effect' => 'fade',
                'progress' => array('type' => 'none'),
            )
        );
    }

    if (!user_is_logged_in()){
        $form['hmmer']['saveDatasetsPlaceholder'] = array(
            '#id' => 'saveDatasetsPlaceholder',
            '#markup' => '<p class="saveDatasetsPlaceholder">Want to save your options? <a href="/user">Login</a> or <a href="/user/register">register here.</a></p>'
        );
    }  

	$form['hmmer']['sequenceUpload'] = array(
			'#type' => 'file',
			'#title' => t('Upload HMMer Input File'),
			'#title_display' => 'before',
			);


	$form['hmmer']['parameters'] = array(
        '#type' => 'fieldset',
        '#title' => t('Parameters'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
	);


	$form['hmmer']['parameters']['basic'] = array(
        '#type' => 'fieldset',
        '#title' => t('Basic'),
	);

  $form['hmmer']['parameters']['basic']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Sequence Type'),
    '#options' => array(
    		//'DNA'		 =>t('DNA'),
    		'PROTEIN' =>t('Protein')),
    '#default_value' => $dFault['type']
  );

	$form['hmmer']['parameters']['basic']['program'] = array(
		'#type' => 'radios',
		'#title' => t('Program'),
		'#options' => array(
			'phmmer' => t('phmmer'),
			'hmmsearch' => t('hmmsearch')),
		'#default_value' => $dFault['program']
	);

	$form['hmmer']['parameters']['basic']['cutoff'] = array(
		'#type' => 'radios',
		'#title' => t('Cut-Offs'),
		'#options' => array(
			'evalue' => t('E-value'),
			'bitscore' => t('Bit Score')),
		'#default_value' => $dFault['cutoff']
	);

	$form['hmmer']['parameters']['cutoffs'] = array(
        '#type' => 'fieldset',
        '#title' => t('Cut-off values'),
	);
	$form['hmmer']['parameters']['cutoffs']['significance'] = array(
        '#type' => 'fieldset',
        '#title' => t('Significance'),
	);
	$form['hmmer']['parameters']['cutoffs']['significance']['sigSequence'] = array(
		'#type' => 'textfield',
		'#title' => t('Sequence'),
		'#size' => 5, 
		'#maxlength' => 5,
		'#default_value' => $dFault['sigSequence']
	);
	$form['hmmer']['parameters']['cutoffs']['significance']['sigHit'] = array(
		'#type' => 'textfield',
		'#title' => t('Hit'),
		'#size' => 5, 
		'#maxlength' => 5,
		'#default_value' => $dFault['sigHit']
	);
	$form['hmmer']['parameters']['cutoffs']['report'] = array(
        '#type' => 'fieldset',
        '#title' => t('Report'),
	);
	$form['hmmer']['parameters']['cutoffs']['report']['reportSequence'] = array(
		'#type' => 'textfield',
		'#title' => t('Sequence'),
		'#size' => 5, 
		'#maxlength' => 5,
		'#default_value' => $dFault['reportSequence']
	);
	$form['hmmer']['parameters']['cutoffs']['report']['reportHit'] = array(
		'#type' => 'textfield',
		'#title' => t('Hit'),
		'#size' => 5, 
		'#maxlength' => 5,
		'#default_value' => $dFault['reportHit']
	);

	$form['hmmer']['parameters']['datasets']=hmmer_generateDatasets();



  $form['hmmer']['results'] = array(
    '#title' => '',
    // The prefix/suffix provide the div that we're replacing, named by
    // #ajax['wrapper'] above.
    '#prefix' => '<div id="results">',
    '#suffix' => '</div>',
    '#type' => 'item',

    '#ajax' => array(
      'callback' => 'ajax_runningJob_handler',
      'event' => 'click',
      'wrapper' => 'test',
      'effect' => 'fade',
      'progress' => array('type' => 'none'))
  );

  $form['hmmer']['result'] = array(
    '#type' => 'item',
  );

  return $form;
}

/**
 * Callback element needs only select the portion of the form to be updated.
 * Since #ajax['callback'] return can be HTML or a renderable array (or an
 * array of commands), we can just return a piece of the form.
 */
function ajax_hmmer_handler($form, $form_state) {
	// 1)validate user input
	// 1.1) check for job id to look up

	global $user;
        //  clear any error css that may have come up
  	$commands[]=ajax_command_css('#edit-sequence', array('border' => 'solid 1px #aacfe4'));
  	$commands[]=ajax_command_css('#edit-comments', array('border' => 'solid 1px #aacfe4'));
	$commands[]=ajax_command_css('#edit-datasets', array('border' => 'solid 1px #aacfe4'));
	$commands[]=ajax_command_invoke('#instructions','hide');
	$commands[]=ajax_command_invoke('#instructions>div','text',array("Everything looks good at first"));

	$message = '';

	if($form_state['values']['lookup']!=''){
		if(preg_match("#^(\d+)$#",$form_state['values']['lookup'],$match))
		{
			$id=$match[1];
      			$jobBeingLookedUp = true;
            $userJob = db_query("select * from xgrid_job_params where argument = 'user_name' and job_id=$id and (value = '$user->name' or value is NULL)");
            if (count($userJob->fetchAll()) < 1){
                $message="HMMER Job ID: $id is either not associated with this account or not a valid job number.  Please login to the proper account to view results if this is your job.";
            }
		}
		else {
			$message="Invalid job ID: ".$form_state['values']['lookup'].". Only integer values are accepted.";
		}

	// 1.2) check for valid data on submitted form
	}else{
		// setup directory for user upload storage
		$filepath = 'public://userHMMer';
		file_prepare_directory($filepath, FILE_CREATE_DIRECTORY);

     		// save user upload locally
        	$hmmerUploadMaxSize = 3*1024*1024; // 3MB file upload limit for HMMer arbitrarily set by ND. Don't think HMMer has a defined limit.
        	// this needs to be used below to ensure correct files are being uploaded. Not used now since we haven't compiles
        	// an exhaustive list of extensions for hmmer.
        	$hmmerFileExtensions = 'fasta fas fa seq fsa fna ffn faa frn txt pir';
        	$validators = array('file_validate_size' => array($hmmerUploadMaxSize),'file_validate_extensions' => array());
        	$file=file_save_upload('sequenceUpload',$validators,$filepath,FILE_EXISTS_RENAME);
		if($file){
			$file = file_move($file, $filepath);
			// get the contents of the file
			$realPath = drupal_realpath($file->uri);
			$form_state['values']['sequence']=file_get_contents($realPath);
			// delete the file
			file_delete($file);
		}
        	else if ($file === FALSE) {
            		$myTempMessages = drupal_get_messages('error');
            		$myMessages = $myTempMessages['error'];
            		foreach( $myMessages as $myError )
            		{
                		$message .= "&bull; " . $myError . "<br/>";
            		}
        	}

		if($form_state['values']['sequence']=='' && $file === NULL){ //add to the instructions message and highlight the sequence input box.
			$message.="&bull; You forgot to enter query sequences. <br/>";
			$commands[]=ajax_command_css('#edit-sequence', array('border' => 'solid red 1px'));

		// input looks good
		}

		//get checked dbs. these will all be digits
		foreach($form_state['values'] as $key => $value){
      			if(preg_match("#\d+#",$key) && $value==1){
                    // value returned from the query is a filename.fa.gz format
        			$query=db_query("select filename from file_managed f, field_data_field_file d where d.entity_id=$key and d.field_file_fid=f.fid")->fetchField();
        			$args['dbs'][]=substr($query,0,-3); // removes the .gz from the filename

        			//Get the database IDs in array to be placed in database later
        			if(isset($args['database_id'])) {
          				$args['database_id'] .= ',' . $key;
        			}
        			else {
          				$args['database_id'] = $key;
        			}
      			}
		}

    // if the 'upload custom dataset' checkbox is checked
	if($form_state['values']['custom_checkbox'] == 1) {

        // handle a custom database file upload
       	$hmmerCustomDBUploadMaxSize = 50*1024*1024; // 50MB file upload limit for HMMer dataset upload set by ND based on timeout concerns and typical size of  PEPTIDES fasta files
        $file=file_save_upload('custom_db_file',array('file_validate_size' => array($hmmerCustomDBUploadMaxSize),'file_validate_extensions' => array('fa fasta')),$filepath,FILE_EXISTS_REPLACE);
        if($file){
            // way of identifying custom datasets is by preprending custom_user_db_ to filename
            $args['dbs'][]='custom_user_db_' . $file->filename;
        }
        else if ($file === FALSE) { // file didn't upload properly. same as above block for hmmer query
            $myTempMessages = drupal_get_messages('error');
            $myMessages = $myTempMessages['error'];
            foreach( $myMessages as $myError )
            {
                $message .= "&bull; " . $myError . "<br/>";
            }
        }
	}
		// fail if no dbs selected
		if(count($args['dbs'])<1){ //add to the instructions message and highlight the datasets
			$message.="&bull; Please select databases to search against <br/>";
			$commands[]=ajax_command_css('#edit-datasets', array('border-color' => 'red'));
		}

            	// check and modify input sequence
            	$foundDNACount = 0;
            	$totalSeqLineCount = 0;
		if($form_state['values']['program']=='hmmsearch'){

              		if( preg_match("/^\s*CLUSTAL O?\(?\d\.\d\d?\.?\d?\d?\)? multiple sequence alignment/i", $form_state['values']['sequence']) != 1 &&
                  	preg_match("/^\s*STOCKHOM \d+\.\d+/i", $form_state['values']['sequence']) != 1)
              		{
                  		$message .= "&bull; hmmsearch expects Clustal format. Please check your input sequence <br/>";
                  		$commands[]=ajax_command_css('#edit-sequence', array('border' => 'solid red 1px'));
              		} else{

              			// hmmsearch - expecting Clustal format.
				// turns the multiline input into a list, split on the newline character.
				$lineList = explode("\n", $form_state['values']['sequence']);
				$finalString = "";
	
				foreach ($lineList as $line)  {
					// get rid of all that extra wasted space. trim all lines and don't send any blank ones after the trim.
					$trimLine = trim($line);
					if(strlen($trimLine) == 0 ) { continue; }
					else {
						// convert clustal format to stockholm
						$patterns = array("/CLUSTAL O?\(?\d\.\d\d?\.?\d?\d?\)? multiple sequence alignment/","/^([\:\*\.\s])+/","/-/");
						$replacements = array("# STOCKHOLM 1.0","\n\n",".");
						$replaceContents = preg_replace($patterns, $replacements, $trimLine);

						// ignore directive lines. Pass them through as-is
						if ($replaceContents[0] == '#') {
							$finalString = $finalString . $replaceContents . "\n";
						// do a three pass replace. 1) split out the sequence name from the actual sequence contents
						// 2) strip all whitespace from what we think is the sequence.
						// 3) put the name and whitespaceless sequence back together using a space.
						} else {
							$sequenceName = preg_replace("/^(\S+)\s+.+/","$1",$replaceContents);
                            				$sequenceContents = preg_replace("/^\S+\s+(.+)/","$1",$replaceContents);
							$sequenceContents = str_replace(" ","",$sequenceContents);
							$finalString .= $sequenceName . " " . $sequenceContents . "\n";

                            				if(preg_match("/^[ACGT\-\*\.\s]+$/i", $sequenceContents) == 1 ){
                               			 		$foundDNACount++;
                            				}
                            				$totalSeqLineCount++;
						}
					}
				}
				// Only add the final two slashes to the file if they aren't already there. Without this check, pure STOCKHOLM formatted files
				// result in two '//' lines at the end of the file, which causes the job to choke due to bad input.
				if( preg_match('/\/\/[\s]*$/',$finalString) == 0) {
					$finalString = $finalString . "//";
				}
				$args['sequence'] = $finalString;
			}
		}
            	else {
                	//phmmer - expecting FASTA format.
                	if( preg_match("/^\s*>.*/",$form_state['values']['sequence'] ) != 1) {
                    		$message .= "&bull; phmmer expects FASTA format. Please check your input sequence <br/>";
                    		$commands[]=ajax_command_css('#edit-sequence', array('border' => 'solid red 1px'));
                	} else {
                		$lineList = explode("\n", $form_state['values']['sequence']);
                		foreach ($lineList as $line)  {
                    			$trimLine = trim($line);
                    			if(strlen($trimLine) == 0 || preg_match("/^>.*/", $trimLine) == 1) { continue; }
                    			else{
                        			if(preg_match("/^[ACGT\-\*\.\s]+$/i", $trimLine) == 1 ){
                            				$foundDNACount++;
                       	 			}
                        			$totalSeqLineCount++;
                    			}
                		}
				$args['sequence']=$form_state['values']['sequence'];
                	}
            	}

            	if($foundDNACount == $totalSeqLineCount && $totalSeqLineCount != 0){
                	$message .= "&bull; Your sequence looks like pure DNA. Please enter a protein sequence. <br/>";
                	$commands[]=ajax_command_css('#edit-sequence', array('border' => 'solid red 1px'));
            	}

        	if( $message == ''){

                	//build argument/value pairs
                	if($form_state['values']['cutoff']=='evalue'){
                    		$args['arguments']['--incE']=$form_state['values']['sigSequence'];
                    		$args['arguments']['--incdomE']=$form_state['values']['sigHit'];
                    		$args['arguments']['-E']=$form_state['values']['reportSequence'];
                    		$args['arguments']['--domE']=$form_state['values']['reportHit'];
                	}
                	else {
                    		$args['arguments']['--incT']=$form_state['values']['sigSequence'];
                    		$args['arguments']['--incdomT']=$form_state['values']['sigHit'];
                    		$args['arguments']['-T']=$form_state['values']['reportSequence'];
                    		$args['arguments']['--domT']=$form_state['values']['reportHit'];
                	}
                	$args['program']=$form_state['values']['program'];
                	$args['sequence_type'] = $form_state['values']['type'];
                	if ($form_state['values']['description'] != '') {
                    		$args['description']=$form_state['values']['description'];
                    } else if (preg_match('/>.*/', $args['sequence'], $matches) === 1) {
                        $args['description'] = substr(trim($matches[0]), 1, 15);
                    }
                    else {
                        $args['description'] = 'No description available';
                    }
                	// submit job
                	$id=hmmer_submit_xgrid($args);
        	}


		
	}

	if($message != '') { //we have errors, show the instructions dialog
		$commands[]=ajax_command_html('#instructions>div',$message);
		$commands[]=ajax_command_invoke('#instructions','fadeIn');
    		$commands[]=ajax_command_invoke('#submissionDialog','dialog',array("close"));
    		$commands[]=ajax_command_invoke(NULL, "scrollToElement",array(t('instructions')));
	}
    	else{
      		// put the id in a hidden div so that javascript can access the job id using jquery
      		// maybe could be changed to a 'drupal_add_js' call with the 'setting' option TRUE?
      		$idText="<div id='parseJobId'>$id</div>";
      		$commands[]=ajax_command_html('#edit-jobid',$idText);

      		//This section is use to populate the parameters when a job is looked up
      		if(isset($jobBeingLookedUp)) {
        		$argumentsToForm = hmmerMapArgumentsToForm();
        		$jobParameters = db_query("select * from xgrid_job_params where job_id=$id");

        		foreach( $jobParameters as $Parameter) {
          			$hmmerArg = $Parameter->argument;
          			$hmmerArgValue = $Parameter->value;
          			if($hmmerArg != 'user_name' && $hmmerArg != 'submitter_ip' &&
            			$hmmerArg != 'program' && $hmmerArg != 'date' && $hmmerArg != 'time') {
	
					if ($hmmerArg == 'sequenceType' || $hmmerArg == 'searchProgram') {	
						$hmmerArgValue = strtolower($hmmerArgValue);
              					$commands[]=ajax_command_invoke($argumentsToForm[$hmmerArg] . $hmmerArgValue, 'attr', array('checked', 'checked'));
            				}
            				elseif ($hmmerArg == 'databaseIds') {
              					//Remove clicked database ids
              					$commands[] = ajax_command_invoke(NULL, 'clearDatasets', array());
              					//separate ids from database
              					$databases = explode(",", $hmmerArgValue);
              					foreach ($databases as $database) {
                					$commands[]=ajax_command_invoke($argumentsToForm[$hmmerArg] . $database, 'attr', array('checked', 'checked'));
              					}
            				}
            				else {
              					if($hmmerArg == '-T') {
                					$commands[]=ajax_command_invoke('#edit-cutoff-bitscore', 'attr', array('checked', 'checked'));
              					}
              					if ($hmmerArg == '-E') {
                					$commands[]=ajax_command_invoke('#edit-cutoff-evalue', 'attr', array('checked', 'checked'));
              					}
              					$commands[]=ajax_command_invoke($argumentsToForm[$hmmerArg], 'val', array($hmmerArgValue));
            				}
          			}
        		}
      		}
	}

	return array('#type' => 'ajax', '#commands' => $commands);
}

function hmmer_getResults($id){
    // should have either a look up id or submitted job id at this point
    if($id){

        try{
            $status=tool_helpers_status($id);
        } catch (Exception $e){
            // there is no status because "error = InvalidJobIdentifier;
            $status="Error";
        }

        // job is done
        if($status==="Completed" && hmmer_isHMMerJob($id)){
            // CHECK TO SEE IF THERE IS ERROR TEXT IN JOB RESULTS
            $jobResults=tool_helpers_results($id);
            if( strpos($jobResults,"Error: ") !== false || strpos($jobResults,"Sequence file sequence is empty or misformatted") !== false){

                //preg_match("#(.*?)====END OF STDOUT====#sm",$jobResults,$errors);
                $errors=preg_replace("/\n+/", "\n", $jobResults);
                $errors=str_replace("\n","<br/>",$errors);
                $results='<fieldset style="padding:4px;border:3px solid red;" >
                <legend style="font-size:12pt;">Results</legend>
                <div style="clear:none;">
                <div style="font-size:12pt;font-weight:bold;margin-bottom:10px;">Job <b id="jobId">'.$id.'</b></div>
                    <div style="float:left;padding:6px;margin:6px;">
                    Yikes! There were some problems running your job. Here is the output from the hmmer program:<br/>
                    '.$errors.'<br/>
                    Please double check your input sequence.<br/>';
                if($form_state['values']['program']=='hmmsearch'){
                    $results.='Sequence lines should include a name and aligned sequence separated by some form of whitespace. <br/>
                    Aligned sequences should not be broken over multiple lines.';
                }

                $results.='</div></fieldset>';
            }else{
                // generate some stats
                $time=tool_helpers_runtime_raw($id);
                $jobDescription=db_query("select value from xgrid_job_params where job_id=$id and argument='description';")->fetchField();

                // display results
                //xgrid_zipResults($id);
                $results='<fieldset style="padding:4px;border:3px solid #b1ca49;" >
                <legend style="font-size:12pt;">Results</legend>
                <div style="clear:none;">
                <div style="font-size:12pt;font-weight:bold;margin-bottom:10px;">Job <b id="jobId">'.$id.'</b></div>';
                if ( $jobDescription != '' )
                    $results.='<b>Description</b>  '.$jobDescription.'
                                <br />';
                $results.='<b>Compute Time</b>&nbsp;&nbsp;'.$time.'&nbsp;seconds<br />
                    <b><a href="/tool_helpers/rest/' . $id . '/results_raw?download=true&filename=hmmer_results_' . $id . '.txt">Download Raw Results</a></b><br/>';

                $resultsLines = explode("\n", $jobResults);

                // add jump to database list with links
                // read all lines

                $counter = 1;
                $databaseList = "";
                $tempResults = "";
                foreach ($resultsLines as $line)
                {
                    // find the beginning of a database set.
                    if(strpos($line,'hmmsearch :: ') !== false || strpos($line,'phmmer :: ') != false){
                        if($counter > 1)
                            // quick navigation to top of results
                            $tempResults .= '<br/><strong><a href="#edit-result">Return to Top</a></strong><br/><br/>';
                            // append the line to temp results with a surrounding span so the javascript can jump here when the 'Jump to Dataset' option changes.
                            $tempResults .= '<span id="D' . $counter . '">' . str_replace(" ", "&nbsp" ,$line) . "</span><br/>";
                    }elseif( strpos($line,'target sequence database:') !== false){ // if the line contains the phrase 'target sequence database:',
                        //  regexp parse the database name
                        //  add the database name as a selectable option
			            $shortDBName = trim(preg_replace('/^#[\s]target sequence database:.+\/(.*)$/',"$1",$line));
                        $databaseList .= '<option value="#D' . $counter . '">' . $shortDBName . "</option>";
                        //  append the line to the temp results string with a extra emphasis on the database name
                        $tempResults .= str_replace("||"," ",str_replace(" ", "&nbsp" ,preg_replace('/(^#[\s]target sequence database:\s+)(.*)$/',"$1<span||style=\"font-weight:bold;color:#005285\"><strong>$shortDBName</strong></span>",$line))) . "<br/>";
                        $counter++;
                    } else {// else
                    //  simply append the line with spaces replaced with &nbsp
                        $tempResults .= str_replace(" ", "&nbsp" ,$line) . "<br/>";
                    }
                }

                $results .= "<b>Jump To Dataset </b><select id=\"datasetSelect\">" . $databaseList . "</select></div>";
                //$results.=str_replace(" ","&nbsp;",$jobResults);


                $results.='<div style="font-family: monospace; font-weight:normal;font-size:12px;padding: 6px;margin-top:10px;">' . $tempResults;

                $results.='</div></fieldset>';

            }
        }else
            $message="Job $id does not contain HMMer results";
    }

    return $message.$results;

}

function hmmer_submit_xgrid($args){
	//generate random name to submit the job as. xgrid_genRandomString is in the xgrid module
	$random=xgrid_genRandomString(6);
	$filename="hmmer_".$random;

	//create program argument string
	foreach($args['arguments'] as $argument => $value){
		$arguments.="\t\t\t<string>$argument</string>\n";
		$arguments.="\t\t\t<string>$value</string>\n";
	}

	//Paranoid checking of input program for security. Make sure we're executing an actual blast program
	$program = $args['program'];
	if (!( $program == "phmmer" || $program == "hmmsearch")) {
		exit();
	}

	$myArguments = array();
	foreach($args['arguments'] as $argument => $value) {
		if($argument!='sequence' && $argument!='dbs' && $argument!='program' && $argument!='description') {
			$myArguments[$argument] = $value;
		}
	}
	$hmmerJobSpec = new JobSpec($program, $args['sequence'], $myArguments, $args['dbs'], $args['description']);
	$lackeyHandle = new HMMerLackey();
	$jobId = $lackeyHandle->submit($hmmerJobSpec);

	//save some job params in the db
	/*global $user;
	$args['arguments']['user_name']=$user->name;
	$args['arguments']['submitter_ip']=$_SERVER['REMOTE_ADDR'];
	$args['arguments']['program']='HMMER';
	$args['arguments']['date']=date("m/d/y");
	$args['arguments']['time']=date("H:i:s");
	$args['arguments']['sequence']=$args['sequence'];
	$args['arguments']['searchProgram']=$args['program'];
	$args['arguments']['description']=$args['description'];
  $args['arguments']['databaseIds']=$args['database_id'];
  $args['arguments']['sequenceType']=$args['sequence_type'];
	xgrid_saveParams($jobId,$args['arguments']);
*/

	return $jobId;
}

function hmmer_isHMMerJob($parseId){

    //first check local db
    $reesult=db_query("select count(*) from xgrid_job_params where job_id=$parseId");
    $results = $reesult->fetchAssoc();
    if(!empty($results['count'])) {
        $result=true;
    }

    // drop this if block after a few months and we've build up the rawid values in the input params db
    if(!$result){
        $tmp=xgrid_specification($parseId);
        $result=preg_match("#name = \"hmmer_#",$tmp);
    }
    return $result;
}

/**
 * Implements hook_block_info().
 */
function hmmer_block_info() {
  $blocks['hmmer_block'] = array(
                          'info' => t('hmmer GUI for submitting jobs to xgrid'),
                          'cache' => DRUPAL_CACHE_GLOBAL,
                          'status' => true,
                          'region' => 'content',
                          'weight' => 0,
                          'visibility' => BLOCK_VISIBILITY_LISTED,
                          'pages' => 'hmmer',
                          );
  return $blocks;
}

/**
 * Implementation of hook_help()
*/
function hmmer_help($path, $arg){
  switch ($path) {
    case "admin/help#hmmer":
      return '<p>'.  t("this is some text from the help hook") .'</p>';
      break;
  }

}

/**
 * Implements hook_menu().
 */
function hmmer_menu(){
  $items = array();

  $items['admin/config/system/hmmer'] = array(
    'title' => 'HMMer settings',
    'description' => 'place holder',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hmmer_configform'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  return $items;
}

/**
 * Form function, called by drupal_get_form() 
 * in hmmer_menu().
 */
function hmmer_configform($form, &$form_state){
  $form['from'] = array(
  '#type' => 'item',
  '#title' => t('hmmer Web Service Settings'),
  '#markup' => 'The (separate) web service runs on an OS X machine which has access to the hmmer program on the command line. 
The service runs over SOAP and implements the basic functionality of the hmmer program.<br/>These settings connect to http://server:port/wsdlPath<br/>Currently: ' . hmmer_getURL(),
);

  $form['hmmer_server'] = array(
    '#type' => 'textfield',
    '#title' => t('hmmer service machine'),
    '#default_value' => variable_get('hmmer_server', 'jobs.vectorbase.org'),
    '#maxlength' => 35,
    '#description' => t('The machine where the hmmer web service is running.'),
    '#required' => TRUE,
  );

  $form['hmmer_server_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('hmmer_server_port', '80'),
    '#maxlength' => 4,
    '#description' => t('The service port.'),
    '#required' => TRUE,
  );

  $form['hmmer_controller_url_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('WSDL Path'),
    '#default_value' => variable_get('hmmer_server_suffix', 'hmmer.wsdl'),
    '#maxlength' => 35,
    '#description' => t('Path to WSDL file on hmmer server. No leading slash.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function hmmer_generateDatasets(){
    global $user;
    $dFault = variable_get(SAVE_DATASETS_VAR_HMMER.$user->uid);
    
	$formOut = array(
        '#type' => 'fieldset',
        '#title' => t('Datasets'),
	);

	// get node ids for files with xgrid enabled
	$query = new EntityFieldQuery();
	$result = $query->entityCondition('entity_type', 'node')
	  ->entityCondition('bundle', 'downloadable_file')
	  ->fieldCondition('field_xgrid_enabled', 'value', '1', '=')
	  ->fieldCondition('field_status', 'value', 'Current', '=')
	  ->execute();



	// as long as we have some dbs enabled, get info about those dbs
	if (isset($result['node'])) {

		// pull out full node info for each of the nids identified by the entityfieldquery
		$nodes=node_load_multiple(array_keys($result['node']));


	 	//first find organisms with xgrid dbs available
		$organisms=array();
		foreach($nodes as $node) {
			$orgTid=$node->field_organism_taxonomy['und'][0]['tid'];
			$orgName=taxonomy_term_load($orgTid);
			$organisms[$orgName->name]=$orgTid;
		}
		// alphabetically sort organisms
		ksort($organisms);

    $formOut['organisms'] = array(
      '#type' => 'fieldset',
      '#attributes' => array(
      'class'=> array('organismsField'),
            ),
      '#prefix' => '<div id="stupidWrapperForFirefox">',
      '#suffix' =>  '</div>',
    );

        // The first item in the list is a 'select all' button to automatically choose all of the datasets.
        $formOut['organisms']['select_all_datasets'] = array(
            '#type' => 'checkbox',
            '#title' => t("<b>Select All Datasets</b> "),
            '#default_value' => $dFault["select_all_datasets"],
        );

		// what is the tid for peptides?
		$peptideTid=taxonomy_get_term_by_name('Peptides');
		$peptideTid=array_shift($peptideTid)->tid;

	 	//second make fieldset of file types available for the organisms
	 	// these will be visible/hidden on organism mouseover/click
		foreach ($organisms as $organism => $orgNid) {

			// now display peptide dbs
			$query = new EntityFieldQuery();
			$result = $query->entityCondition('entity_type', 'node')
			  ->entityCondition('bundle', 'downloadable_file')
			  ->fieldCondition('field_xgrid_enabled', 'value', '1', '=')
			  ->fieldCondition('field_download_file_type', 'tid', $peptideTid, '=')
			  ->fieldCondition('field_organism_taxonomy', 'tid', $orgNid, '=')
			  ->fieldCondition('field_status', 'value', 'Current', '=')
			  ->execute();

			//get all the dbs info
			if(isset($result['node'])){

				$dbs=node_load_multiple(array_keys($result['node']));
				foreach($dbs as $db){
					$nid=$db->nid;
					$fileDescription=$organism . ', ';	
					$a = explode(',', trim($db->field_description['und'][0]['value']));
					if(empty($a) || count($a) != 2) {
						$fileDescription = substr($fileDescription, 0, -2);
					} else {
						$b = explode('strain', $a[0]);
						if(!empty($b)) {
							$fileDescription.=$b[0];
							$fileDescription.=' strain, ';
						}
						$fileDescription.= $a[1];
					}
					// file type will be peptide
					$formOut['organisms'][$organism]['Peptide'][$nid] = array(
						'#type' => 'checkbox',
						'#title' => t("<b>Peptides</b> ".$fileDescription),
                        '#default_value' => $dFault[$nid],
						'#attributes' => array(
							'class' => array('pepDbs', 'dbs'),
							'data-org' => array(str_replace(" ","_", $organism))
						)						
					);
				}
			}
		}

        // only show the custom database upload checkbox/file upload form if it is a logged in user
        if( user_is_logged_in() ) {
                $formOut['organisms']['upload_custom_dataset']['Peptide']['custom_checkbox'] = array(
                    '#type' => 'checkbox',
                    '#title' => t("<b>Upload custom dataset</b> "),
                    '#default_value' => $dFault['custom_checkbox'],
                    '#attributes' => array(
                        'class' => array('custom', 'dbs') //adds some styling
                    )
                );

                $formOut['organisms']['upload_custom_dataset']['Peptide']['custom_db_file'] = array(
                    '#type' => 'file',
                    '#title_display' => 'invisible',
                );
        }

	} // end checking for any xgrid enabled dbs
//dpm(blast_dbinfo('agambiae.TRANSCRIPTS-AgamP3.6.fa'));
	return $formOut;
}

function hmmerMapArgumentsToForm() {
  $argumentsToForm = array();

  $argumentsToForm['--incE'] = '#edit-sigsequence';
  $argumentsToForm['--incdomE'] = '#edit-sighit';
  $argumentsToForm['-E'] = '#edit-reportsequence';
  $argumentsToForm['--domE'] = '#edit-reporthit';
  $argumentsToForm['--incT'] = '#edit-sigsequence';
  $argumentsToForm['--incdomT'] = '#edit-sighit';
  $argumentsToForm['-T'] = '#edit-reportsequence';
  $argumentsToForm['--domT'] = '#edit-reporthit';

  $argumentsToForm['description'] = '#edit-description';
  $argumentsToForm['sequence'] = '#edit-sequence';
  $argumentsToForm['sequenceType'] = '#edit-type-';
  $argumentsToForm['searchProgram'] = '#edit-program-';
  $argumentsToForm['databaseIds'] = '#edit-';

  return $argumentsToForm;
}

function save_datasets_hmmer($form, &$form_state) {
//    file_put_contents("/vectorbase/web/root/sites/all/modules/custom/abc.txt", print_r($form_state['values'], TRUE));
    $args = $form_state['values'];
    global $user;
    variable_set(SAVE_DATASETS_VAR_HMMER.$user->uid, $args);
    return false;
}

function delete_datasets_hmmer ($form, &$form_state) {
    global $user;
    variable_del(SAVE_DATASETS_VAR_HMMER.$user->uid);
    return false;
}
