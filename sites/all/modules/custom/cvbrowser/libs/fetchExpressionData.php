<?php

print " ";
return;


require_once( "dbFunctions.php" );

$db = connectToDb();

$xrefId = $db->real_escape_string( $_GET[ 'xid' ] );

$q = "SELECT * FROM cv_term WHERE xref_id='$xrefId'";
$qr = $db->query( $q );
$row = $qr->fetch_assoc();

if( $row === FALSE )
{
        print "No data found for this ID.";
        return;
}

$termId = $row['id'];
$termName = $row['name'];

$url="https://pre.vectorbase.org/vbsearch_facets?terms=$termName";
//print $url;
$conn = fopen( $url, "r" );
$response = "";

if( $url )
{
	while( $line = @fgets( $conn, 1024 ) )
	{
		$response .= $line;
	}
}

fclose( $conn );

//print $response;

$a = json_decode( $response, true );

//var_dump( $a );

if( count( $a['sites'] > 0 ) )
{
	print "<table style='width: 100%'>";
	print "<tr><td align='center' style='border-bottom: solid 3px black;'><b>Domain</b></td><td align='center' style='border-bottom: solid 3px black;'><b>Hits</b></td></tr>";

	foreach( $a['sites'] as $site=>$value )
	{
		print "<tr><td valign='top' style='border-bottom: solid 1px black;'><a href=\"".$a['sites'][ $site ]['url']."\">".$site."</a></td>";
		print "<td valign='top' style='border-bottom: solid 1px black;'>".$a['sites'][ $site ]['count']."</td></tr>";
	}

	print "</table>";
}

print "<br><br>";

if( count( $a['species'] > 0 ) )
{
	print "<table style='width: 100%'>";
	print "<tr><td align='center' style='border-bottom: solid 3px black;'><b>Species</b></td><td align='center' style='border-bottom: solid 3px black;'><b>Hits</b></td></tr>";

	foreach( $a['species'] as $species=>$value )
	{
		print "<tr><td valign='top' style='border-bottom: solid 1px black;'><i><a href=\"".$a['species'][ $species ]['url']."\">".$species."</a></i></td>";
		print "<td valign='top' style='border-bottom: solid 1px black;'>".$a[ 'species' ][ $species ][ 'count' ]."</td></tr>";
	}

	print "</table>";
}

