<?php

require_once( "dbFunctions.php" );
$db = connectToDb();

$xrefId = $db->real_escape_string( $_GET['xrefId' ] );

$q = "SELECT name, descendant_xref_id FROM cv_term, cv_term_relationship WHERE ancestor_xref_id='$xrefId' AND xref_id=descendant_xref_id ORDER BY NAME ASC";
$qr = $db->query( $q );

$rowsCount = $qr->num_rows;

if( $rowsCount == 0 )
{
	exit();
}

$response = "";

$row = $qr->fetch_assoc();

while( $row != FALSE )
{
	$response = $response . $row['descendant_xref_id']."|".$row['name']."|";
	
	$tq = "SELECT DISTINCT(descendant_xref_id) FROM cv_term_relationship WHERE ancestor_xref_id='".$row['descendant_xref_id']."'";
	$tqr = $db->query( $tq );
	
	$rowsCount = $tqr->num_rows;
	
	if( $rowsCount > 0 )
	{
		$response = $response . $rowsCount;
	}
	else
	{
		$response = $response . "0";
	}

	$response = $response . "<br>";

	$row = $qr->fetch_assoc();
}

print $response;
