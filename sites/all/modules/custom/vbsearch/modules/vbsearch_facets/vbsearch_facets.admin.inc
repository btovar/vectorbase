<?php

/**
 * Implements hook_form()
 *
 * Constructs the form that will be used to create the different facets that
 * will be used.
 */
function vbsearch_facets_edit_form($form, &$form_state, $vbsearch_facet = array()) {
  if(empty($vbsearch_facet)) {
    unset($vbsearch_facet);
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('VectorBase Facet Name'),
    '#default_value' => isset($vbsearch_facet) ? $vbsearch_facet['label'] : '',
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#defaultl_value' => isset($vbsearch_facet) ? $vbsearch_facet['description'] : '',
    '#required' => TRUE,
  );

  $form['title'] = array(
    '#description' => t('The title of the VectorBase Search facet. Defaults to Facet Name if none specified.'),
    '#type'=> 'textfield',
    '#title' => t('VectorBase Facet Title'),
    '#default_value' => isset($vbsearch_facet) ? $vbsearch_facet['title'] : '',
    '#required' => FALSE,
  );

  $form['facet_categories']['#tree'] = TRUE;

  $facet_categories = vbsearch_facets_get_categories($vbsearch_facet['vbsearch_facet_id']);

  //Build the rows of the table that will display the categories belonging to
  //the facet
  $facet_categories = array();
  foreach ($facet_categories as $facet_category) {
    //Uses the facet_category_id, but could make schema better to give a unique
    //id of the category belonging to the facet
    $form['facet_categories'][$facet_category->facet_category_id] = array(
      'name' => array(
        '#type' => 'markup',
        '#value' => $facet_category->label,
      ),
      'weight' => array(
        '#type' => 'weight',
        '#delta' => 10,
        '#default_value' => $facet_category->weight,
        '#attributes' => array('class' => array('weight')),
      ),
    );
  }

  $form['facet_categories']['add_new_cateogry'] = array(
    'name' => array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Add new category'),
    ),
    'weight' => array(
      '#type' => 'weight',
      '#delta' => 10,
      '#attributes' => array('class' => array('weight')),
    ),
  );

  $form['facet_categories']['add_existing_cateogry'] = array(
    'name' => array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Add exisiting category'),
    ),
    'weight' => array(
      '#type' => 'weight',
      '#delta' => 10,
      '#attributes' => array('class' => array('weight')),
    ),
  );


  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['save'] = array(
    '#type' =>'submit',
    '#submit' => array('vbsearch_facets_edit_form_submit'),
    '#value' => t('Save'),
  );

  if (!empty($vbsearch_facet)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#submit' => array('vbsearch_facets_delete'),
      '#value' => t('Delete'),
    );
  }

  // Ensures destination is an internal URL, builds "cancel" link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = $_GET['destination'];
  }
  else {
    $destination = 'admin/config/search/vbsearch/vbsearch_facets';
  }

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $destination,
  );

  drupal_set_message("Here");

  return $form;
}

/**
 * Implements hook_theme()
 *
 * Creates the theme for vbsearch_facets_edit_form that will allow the table
 * rows to be draggable to set the weight of the categories belonging to the
 * facet.  The weights are used to order the categories when displayed.
 *
 * @return array
 */
function vbsearch_facets_theme() {
  drupal_set_message("I am here");
  /*return array(
    'vbsearch_facets_edit_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );*/
  return array();
}

function theme_vbsearch_facets_edit_form($form) {
  drupal_set_message("I am here 2");
  // The variable that holds our form HTML output
  $output = '';

  //loop through each "row" in the table array
  foreach($form['facet_categories'] as $id => $row) {
    //if $id is not a number skip this row in the data structure
    if (!intval($id))
      continue;

    // this array will hold the table cells for a row
    $this_row = array();


    //first, add the "name" markup
    $this_row[] = drupal_render($row['name']);

    //Add the weight field to the row
    //The javascript to make our table drag and drop will hide the cell
    $this_row[] = drupal_render($row['weight']);

    //Add the row to the arrays of rows
    $table_rows[] = array('data' => $this_row, 'class'=>'draggable');
  }

  //Make sure the header count matches the column count
  $header = array(
    'Name',
    'Weight',
  );

  $table_id = 'vbsearch_categories';

  //This funciton is what brings in the javascript to make our table
  //drag-and-droppable
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'weight');

  //Over-write the 'my_items' form element with the markup generated from our
  //table
  $form['vbsearch_categories'] = array(
    '#type' => 'markup',
    '#value' => theme('table', $header, $table_rows, array('id' => $stable_id)),
    '#weight' => '1',
  );

  //render the form
  $output = drupal_render($form);

  return $output;
}

/**
 * Constructs the VectorBase Facets administration page that will allow the site
 * administrators to configure the different facets that are used in the site.
 */
function vbsearch_facets_admin_page() {
  $headers = array(
    array('data' => t('VectorBase Facet Name'), 'colspan' => 1),
    array('data' => t('Description'), 'colspan' => 1),
    array('data' => t('Operations'), 'colspan' => 3),
  );

  //Get the VectorBase Facets that were created 
  $vbsearch_facets = vbsearch_facets_get_facets();
  dpm($vbsearch_facets);

  if (count($rows)) {
    $output['vbsearch_facets_admin_page'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    );
  }
  else {
    $output = t("No VectorBase Facets were found.");
  }

  return $output;
}

