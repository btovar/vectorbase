(function (VectorBaseSearch, $, undefined) {

  if (VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  VectorBaseSearch.init_filters = function() {
    $('.facetsort').tablesorter();
    $('th a').click( function() {
      window.location = this.href;
    });
  };

  $(document).ready(function () {
    VectorBaseSearch.init_filters();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);
