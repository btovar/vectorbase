<?php
// Adds button and javascript to search form
function vbsearch_md_form_alter(&$form, &$form_state, $form_id) {

	if ($form_id == 'apachesolr_search_custom_page_search_form'){
		// Change the hover message if the user is anonymous vs. logged in.
		if (user_is_logged_in()){
			$markup = '<button id="myBtn" title="Available for 30,000 or fewer results" onclick="Get_Download();"'
				. 'class="form-submit" style="width:125px;font-size:14px;font-weight:bold;">'
				. 'Export results </button></br></br>';
		}else{
			$markup = '<button id="myBtn" title="Available for 30,000 or fewer results. '
				. 'Anonymous users are limited to the first 5000. Login to get more results"'
				. ' onclick="Get_Download();" class="form-submit"'
				. ' style="width:125px;font-size:14px;font-weight:bold">'
				. 'Export results </button></br></br>';
		}	

		// Add in the Javascript to control the button
		drupal_add_js(drupal_get_path('module', 'vbsearch_md') . '/js/md.js');

		// Add the button to the search form
		$form['basic']['vbsearch_md'] = array(
			'#markup' => $markup);
  }
}

function vbsearch_md_menu() {
	$items = array();
	$items['vbsearch/download'] = array(
        'title' => 'Mass Download for VectorBase search',
        'page callback' => 'vbsearch_md_return_data',
        'access arguments' => array('access content'),
	'description' => 'Returns search results as CSV download',
        'type' => MENU_CALLBACK,
        );
	$items['vbsearch/download/timeout'] = array(
	'title' => 'Download Error',
	'page callback' => 'vbsearch_md_timeout',
	'access arguments' => array('access content'),
	'description' => 'Reports timeout of mass download',
	'type' => MENU_CALLBACK,
	);
	return $items;
}

function vbsearch_md_timeout(){
	drupal_add_js(drupal_get_path('module', 'vbsearch_md') . '/js/mdTimeout.js');
	$Message = 'Sorry, but your download request has timed out.';
	return t($Message);
}

function vbsearch_md_return_data (){
	// Production code

	// Step 1: Get the Solr query from the session. Set the session variable to null.
	$solrQuery = $_SESSION['solr_query'];
	$fqarray = $solrQuery->getParam('fq');
	$_SESSION['solr_query'] = null;

	// Step 2: Alter parameters
	// Set the download count depending on login status.
	if (user_is_logged_in()){
		$rowcount = 30000;
	}else{
		$rowcount = 5000;
	}
	
	$solrQuery->replaceParam('rows', $rowcount);
	$solrQuery->replaceParam('hl', 'false');
	$solrQuery->replaceParam('facet', 'false');
	$solrQuery->replaceParam('fl',getFieldset($fqarray));
	//$solrQuery->replaceParam('fl','');
	//$solrQuery->replaceParam('fl', array('label','description','accession','species','projects'));

	// Step 3: Increase PHP time limits to 5 minutes.
	drupal_set_time_limit(300);
	$oldTO = ini_get('default_socket_timeout');
	ini_set('default_socket_timeout', 300);
	$oldExTime = ini_get('max_execution_time');
	ini_set('max_execution_time', 300);

	// Step 4: Execute the query
	try {
		$solrResults = $solrQuery->search();
	}
	catch (Exception $e) {
		if($e->getCode() == 0) {
			drupal_goto('vbsearch/download/timeout');
			return;
		}
	}

	// Step 5: Process the results, creating csv
	// Holds the CSV for each data line
	$solrCSV = "";
	// Holds the CSV header line
	$header = "";
	// Temporary storage for the key values pairs returned by Solr.
	// Required to preserve the order of the columns, as Solr does not
	// maintain column order from row to row for some reason.
	$dataArray = array();
	
	// Get the array of docs
	$docs = $solrResults->response->docs;

	//Set the document headers
        header('Content-Disposition: attachment; filename="download.csv"');
        header("Content-Transfer-Encoding: ascii");
        header('Expires: 0');
        header('Pragma: no-cache');

	// Get the first row and set the column headers of the CSV.
	$rowone = $docs[0];
	foreach ($rowone as $key => $value){
		$header = $header . $key . ",";
		// Here we are setting just the keys for each row.
		$dataArray[$key] = '';
	}
	$solrCSV = $header . "\r\n";
	print($solrCSV);
	$solrCSV = '';

	// Loop through each document and its fields, printing each line out to http.
	foreach ($docs as $doc){
		foreach ($doc as $key => $value){
			if (!is_array($value)){
				// If not an array, assign the value to the corresponding element of dataArray
				$dataArray[$key] = clean($value);
			}else{
				// Loop through the array and build a string in the corresponding element of dataArray
				foreach ($value as $element){
					// Concatenates the array element to the dataArray value
					$dataArray[$key] = $dataArray[$key] . clean($element) . ";"; 
				}
			}
		}
		// Loop through dataArray and build the CSV string. Clear dataArray elements as they are used.
		// This preserves column order from row to row.
		foreach ($dataArray as $key => $datum){
			$solrCSV = $solrCSV . $datum . ",";
			$dataArray[$key] = '';	
		}
		// Add carriage return and line feed.
		$solrCSV = $solrCSV . "\r\n";
		// Print resulting CSV line
	        print($solrCSV);
		// Clear the CSV line for the next set of data
	        $solrCSV = '';

	}

	// Reset timeout to previous value
	ini_set('default_socket_timeout', $oldTO);
	ini_set('max_execution_time', $oldExTime);
	return;
}

function clean($string){
	return preg_replace('/[^A-Za-z0-9\-\." "\/\=\?\_\:\%]/', ' ', $string); // Removes special chars.
}

function getFieldset($fqarray){
    $domain = null;
    $subdomain = null;

    foreach ($fqarray as $fqterm){
        if(is_null($domain)){
            $domain = extractParam($fqterm,'site:"');
        }
        if(is_null($subdomain)){
            $subdomain = extractParam($fqterm,'bundle_name:"');
        }
    }
    
    switch ($domain) {
        case "Population Biology":
            switch ($subdomain) {
                case "Project":  
                    $fieldList = array('authors','date','description','accession','study_designs','label');
                    break;
                case "Sample":
                    $fieldList = array('species','pubmed','date','sample_type','projects','geolocations','geo_coords');
                    break;
                case "Assay":
                    $fieldList = array('species','pubmed','date','sample_type','projects','geolocations','geo_coords','collection_protocols','assay_type');
                    break;
                case "Insecticide resistance assay":
                    $fieldList = array('species','pubmed','collection_date','sample_type','projects','geolocations','geo_coords','collection_protocols','assay_type','insecticides_cvterms');
                    break;
                default:
                    $fieldList = array('authors','collection_date','description','accession','study_designs','label');
            } 
            break;
        case "Expression":
            $fieldList = array('label','description','gene_ids','high_conditions','low_conditions','projects','species');
            break;
        default:
            $fieldlist = array('label','description','accession','species','projects');
    }
    return($fieldList);
}

function extractParam($string,$name){
    if(strpos($string,$name) === false){
        return(null);
    }else{
        $start = strpos($string,$name);
        //print($start);
        $startadj = $start + strlen($name);
        $end = strpos($string,'"',$startadj);
        return(substr($string,$startadj,$end-$startadj));
    }
}

