(function (VectorBaseSearch, $, undefined) {

  if (VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  VectorBaseSearch.init_vbsearchrest = function() {
    jQuery('#executePost').click(function(event) {
      jQuery.ajax({
        type: 'POST',
        url: '/vbsearch/rest/create',
        dataType: 'json',
        data: {"query" : "*",
          "limit" : 1,
          "filter" : ["site:\"Population Biology\"", "bundle_name:Sample",  "species:(\"Aedes aegypti\" OR \"Anopheles gambiae\") OR species_cvterms:(\"Aedes aegypti\" OR \"Anopheles gambiae\")" ]},
        success: function(data) {
          window.alert(data);
        },
        error: function(error) {
          window.alert(error);
        }
      });
    });
  };

  $(document).ready(function () {
    VectorBaseSearch.init_vbsearchrest();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);
