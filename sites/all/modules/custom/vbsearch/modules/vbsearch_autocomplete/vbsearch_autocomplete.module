<?php

/**
 * Implements hook_menu()
 *
 * This function allows us to create the URL that will be used to to give
 * autcomplete suggestions to our VectorBase Search page.
 * @return array
 */

function vbsearch_autocomplete_menu() {
  $items = array();

  //Generates the URL that we will use get our autcomplete suggestions.
  $items['vbsearch/autocomplete/%'] = array(
    'title' => 'VectorBase Search Autocomplete',
    'page callback' => 'vbsearch_autocomplete',
    'page arguments' => array(2,3),
    'access callback' => TRUE,
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Wrapper function used to generate solr query.
 *
 * @param $query
 *  The query to be executed
 *
 * @param $qf
 *  The fields that will be used to search
 *
 * @param $fl
 *  The fields that will be returned
 *
 * @param $url
 *  The url that will be used to do the search
 *
 * @param $mm
 *  Number of terms that need to match to return as a hit
 *  Default is at least 1 term needs to match
 *
 * @return solrBaseQuery
 *  The query object that will be used to search
 */

function vbsearch_createSolrQuery($query, $qf=array('text'), $fl=array(), $mm=1, $url='') {
  // Step 1: Get Apachsolr object
  try{
    $solr = apachesolr_get_solr();
  } catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
  }

  // Step 2: Set the search name
  $name = 'apachesolr';

  // Step 3: Set the params (typically q and fq)
  //$params = array('start' => 0,'rows' => 10,'spellcheck' => 'true', 'wt' => 'json', 'fq' => array());
  $params = array('start' => 0,'rows' => 50, 'wt' => 'json', 'fq' => array());
  #'hl' => 'true', 'hl.mergeContigous' => 'true',
  #  'hl.fragsize' => 999, 'hl.snippets' => 5, 'wt' => 'json', 'fq' => array());

  // Step 4: Set the sortstring
  $sortstring = '';

  // Step 5: Set the base path
  $base_path = "";

  // Step 6: Instanciate the SolrBaseQuery
  // Should set this URL through some form of UI
  $solr->setUrl($url);
  #dpm($solr->getUrl());
  $solrQuery = new SolrBaseQuery($name, $solr, $params, $sortstring, $base_path);

  $solrQuery->addParam('defType', 'edismax');

  $solrQuery->addParam('q', $query);
  $solrQuery->addParam('mm', $mm);

  foreach ($qf as $value) {
    $solrQuery->addParam('qf', $value);
  }

  foreach ($fl as $value) {
    $solrQuery->addParam('fl', $value);
  }

  //Quick way of adding boosting to fields
  //$solrQuery->addParam('bq', 'site="Genome"^35');
  //$solrQuery->addParam('bq', 'site="General"^6');
  //$solrQuery->addParam('bq', 'site="Ontology"^1');

  return $solrQuery;
}

/**
 * This function is what does a SOLR query, parses the response, and sends the
 * parsed suggestions back to search.
 *
 */

function vbsearch_autocomplete($query, $field='') {
  $searchQuery = escapeColon($query) . '*';
  $searchSuggestions = array();
  $queryTerms = explode(' ', $query);
  $lastTerm = end($queryTerms);
  $patternString = '';

  //If getting suggestions from the main search box, use all the searchable
  //fields to look for suggestions, otherwise only search in the fields
  //used by the field to look for suggestions
  if($field == '') {
    //Adding the different solr parameters that were configured to the $query variable
    //Might need to change this in the future if we decide the modify what parameters can get boosts
    /*
    if (file_exists('/vectorbase/web/root/data/configuration/solr_search_fields.txt')) {
      $solr_fields_data = file('/vectorbase/web/root/data/configuration/solr_search_fields.txt');
    } else {
      $solr_fields_data = array();
    }

    foreach($solr_fields_data as $values) {
      $configuration = explode(':', $values);
      $parameters = explode(',', $configuration[2]);
      foreach($parameters as $parameter){
        if ($parameter == 'qf' && $configuration[0] != 'date') {
          if ($configuration[3] != '') {
            $solrQuery->addParam($parameter, $configuration[0] . '^' . $configuration[3]);
          } else {
            $solrQuery->addParam($parameter, $configuration[0]);
          }
          $solrQuery->addParam('fl', $configuration[0]);
        }
      }
    }*/

    $queryParameters = array(
      'fl' => array('text', 'species'),
      'qf' => array('text', 'species'),
    );
  } else {
    //Get configuration of the field (includes the solr mappings)
    //NOTE: It might be necessary in the future to set the VBSearch type through
    //a dynamic way
    $field_config = vbsearch_type_field_load($field, 'advanced_search');
    $queryParameters = array(
      'fl' => array(),
      'qf' => array(),
    );

    if (!empty($field_config['solr_mapping'])) {
      foreach(explode("\n", $field_config['solr_mapping']) as $solr_field) {
        array_push($queryParameters['qf'], $solr_field);
        array_push($queryParameters['fl'] , $solr_field);
      }
    }
  }

  $solrQuery = vbsearch_createSolrQuery($searchQuery, $queryParameters['qf'], $queryParameters['fl'], count($queryTerms), "http://baltar:8080/solr/autocomplete");
  $response = $solrQuery->search();

  //Generate the pattern string to search for in the response
  foreach($queryTerms as $queryTerm) {
    if($queryTerm == $lastTerm) {
      $queryTerm = str_replace(array('*', '\\'), '', $queryTerm);
      $patternString = $patternString . "$queryTerm" . "\S*[^:;\.\)\],\s]";
    } else {
      //pay attention to the space in the end of the string, it was cuasing me
      //problems when matching
      $patternString = $patternString . "$queryTerm\s";
    }
  }

  //We go through the response and parse the results to find
  //text that matches what the user typed in a field
  $results = $response->response->docs;
  foreach($results as $result) {
    foreach($result as $result_fields) {

      //Docs fields do not always return an array.
      //Check if it is an array to loop through otherwise
      //get the value to see if we can get suggestions
      if (is_array($result_fields)) {
        foreach($result_fields as $field_value) {
          preg_match('/' . $patternString . '/i', $field_value, $matches);
          $matches = array_map('strtolower', $matches);
          #$matches = array_map('trim', $matches);
          #$matches = preg_replace('/^\PL+|\PL\z/', '', $matches);
          #$matches = preg_replace("/[^:\sA-Za-z0-9]\.*:*\Z/", '', $matches);
          #$matches = preg_replace("/[^(\w+\W*\w+)]/", '${1}', $matches);
          $searchSuggestions = array_unique(array_merge($searchSuggestions, $matches));
          if (count($searchSuggestions) == 4) {
            break 3;
          }
        }
      } else {
        $field_value = $result_fields;
        preg_match('/' . $patternString . '/i', $field_value, $matches);
        $matches = array_map('strtolower', $matches);
        #$matches = array_map('trim', $matches);
        #$matches = preg_replace("/[^:\sA-Za-z0-9]\.*:*\Z/", '', $matches);
        #$matches = preg_replace("/[^(\w+\W*\w+)]/", '', $matches);
        #$matches = preg_replace('/^\PL+|\PL\z/', '', $matches);
        $searchSuggestions = array_unique(array_merge($searchSuggestions, $matches));
        if (count($searchSuggestions) == 4) {
          break 2;
        }
      }
    }
  }

  $query = strtolower($query);
  foreach($searchSuggestions as $key => $searchSuggestion) {
    //$searchSuggestions[$key] = str_replace($query, $query . '<b>', strip_tags($searchSuggestion)) . '</b>';
    $searchSuggestions[$key] = strip_tags($searchSuggestion);
    if (strpos($searchSuggestion, ':') !== false) {
      $searchSuggestions[$key] = strtoupper($searchSuggestion);
      #$splitSuggestion = explode(':', $searchSuggestion);
    }
  }

  return drupal_json_output($searchSuggestions);
 }

function vbsearch_autocomplete_form_alter(&$form, &$from_state, $form_id) {
  if ($form_id == 'search_block_form') {
    //Adding the JS that wil lbe used for AutoComplete
    // Add the jQuery UI autocomplete library.
    drupal_add_library('system', 'ui.autocomplete');
    $form['#attached']['js'][] = array(
        'data' => drupal_get_path('module', 'vbsearch_autocomplete') . '/vbsearch_autocomplete.js',
    );
    $form['#attached']['css'][] = array(
      'data' => drupal_get_path('module', 'vbsearch_autocomplete'). '/vbsearch_autocomplete.css',
    );
  }
}
