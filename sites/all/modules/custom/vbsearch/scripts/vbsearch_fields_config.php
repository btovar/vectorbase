<?php
$_SERVER['HTTP_HOST'] = gethostname();
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

chdir('/vectorbase/web/root/');
define('DRUPAL_ROOT', getcwd());
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$file = file('/vectorbase/web/root/data/configuration/configuration.txt');

foreach ($file as $line) {
  $fields_configuration = explode(',', $line);
  $field_name = $fields_configuration[0];
  $visible_array = explode(':', $fields_configuration[3]);
  $search_type = $fields_configuration[1];
  $solr_mappings = explode(':', $fields_configuration[2]);
  $placeholder = $fields_configuration[4];
  $visible_detail = array();

  foreach($visible_array as $visible) {
    $visible_detail_array = explode('|', $visible);
    $visible_detail[$visible_detail_array[0]] = explode(';', $visible_detail_array[1]);
  }
  print("Field name: ");
  print($field_name);

  print_r($visible_array);
  print_r($visible_detail);
  print($search_type);
  print($placeholder);
  print_r($solr_mappings);

  $vbfid = db_insert('vbsearch_Field_config')->fields(array('vbsearch_type_name' => $search_type, 'vbsearch_field_name' => $field_name, 'field_solr_mappings' => serialize($solr_mappings), 'vbsearch_field_visibility' => serialize($visible_detail), 'vbsearch_field_placeholder' => $placeholder))->execute();
  //$vbsfid = db_insert('vbsearch_solr_field_config')->fields(array('solr_field_name' => $field_name, 'solr_field_parameters' => serialize($parameters)))->execute();

  /*db_merge('vbsearch_solr_field_config')
    ->key(array('solr_field_name' => $field_name))
    ->fields(array(
      'solr_field_parameters' => serialize($parameters),
      'content_field_solr_mappings' => $content_type_mappings,
    ))
    ->execute();*/
}
?>
