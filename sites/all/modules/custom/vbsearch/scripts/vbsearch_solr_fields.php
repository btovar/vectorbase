<?php
$_SERVER['HTTP_HOST'] = gethostname();
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

chdir('/vectorbase/web/root/');
define('DRUPAL_ROOT', getcwd());
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$file = file('/vectorbase/web/root/data/configuration/solr_search_fields.txt');

foreach ($file as $line) {
  $solr_fields = explode(':', $line);
  $field_name = $solr_fields[0];
  $parameters = explode(',', $solr_fields[2]);
  $content_type_mappings = array();
  list($key, $value) = array_filter(explode('|', $solr_fields[5]));

  //print($field_name);
  //print_r($parameters);
  if (empty($value)) {
    $content_type_mappings = NULL;
  } else {
    $content_type_mappings[trim($key)] = trim($value);
    print_r($content_type_mappings);
    $content_type_mappings = serialize($content_type_mappings);
  }

  /*$vbsfid = db_insert('vbsearch_solr_field_config')->fields(array( 
    'solr_field_name' => $field_name,
    'solr_field_parameters' => serialize($parameters),
    'content_field_solr_mappings' => serialize($content_type_mappings),
  ))->execute();*/
  db_merge('vbsearch_solr_field_config')
    ->key(array('solr_field_name' => $field_name))
    ->fields(array(
      'solr_field_parameters' => serialize($parameters),
      'content_field_solr_mappings' => $content_type_mappings,
    ))
    ->execute();
}
?>
