# Procedure to use the new locentry module with the rectangular geospatial search
    for updating locentry_update_7100() at locentry.install

1. get the new locentry module (e.g., git pull)

2. run update.php to accept database change for locentry module (related to locentry.install)

3. Add a new solr field via Drupal admin (advanced search)
    https://xxx.vectorbase.org/search/site/*?as=True#overlay=admin/structure/vbsearch/manage/advanced_search/fields
    - inputs are like below:
        Search by rectangular geo-location      field_search_rect_geo_location      Location Entry      Location Entry Widget       edit    delete  config

3-1. change config for the new field!!! - press config link at the new field
    - Solr Fields Mapping
        geo_coords
    - Visibility Behavior
        field_search_site|Population Biology;Population Biology-Sample;Population Biology-Sample phenotype;Population Biology-Sample genotype

4. flush all cashes, just in case

5. test
    - go to Advanced search
    - select Population Biology at Domain/Sub-Domain
    - press +Add field and check whether "Search by rectangular geo-location" checkbox exists
    - if exists, select its checkbox. It will show one input text field and a default google map around South Bend with a green box and two markers
    - input coordinates below which is around Spain
        36.980469,-9.043121,38.185811,-7.684937
    - press Go for search and see results are narrowed down: currently it shows 37 results only
