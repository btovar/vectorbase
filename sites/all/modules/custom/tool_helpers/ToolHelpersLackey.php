<?php

use VectorBase\ToolHelpers\JobSpec;
//require_once(drupal_get_path('module', 'tool_helpers'). '/Lackey.php');
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');
class ToolHelpersLackey extends VectorBase\ToolHelpers\Lackey {

  //Making a wrapper to the lackey_save_params so it can be used by the
  //ToolHelpersLackey object
  public function save_job_run_info($jobID, $args) {
    $this->lackey_save_params($jobID, $args);
  }

  //Defined it only because it is required
  protected function buildSubmitFiles(JobSpec $job_specification, $prefix) {
    return "";
  }
}
