<?php
// DKDK this is for VB-4394, technically deleting a taxanomy vocabulary
// taxonomy_vocabulary_machine_name_load("machine_name_in_drupal")
// change "machine_name_in_drupal" for another use
// usually the machine name can be found at "Edit" of the target vocabulary via admin interface
// e.g., File Types ==> file_types

$vocab=taxonomy_vocabulary_machine_name_load("file_types");
echo $vocab->vid;
taxonomy_vocabulary_delete($vocab->vid);

?>