<?php

// define data format
DEFINE('EXPORT_DATE_FORMAT', 'Y-m-d\TH:i:s\Z');

// fetch desired node data
$sql = "select tid from {taxonomy_term_data} where vid=17 order by tid asc";
//$sql = "select nid from {node} order by nid asc";

$count = 0;
$resource = db_query($sql);
$data = new StdClass();
$data->add = new StdClass();
foreach ($resource as $row) {
  $term = taxonomy_term_load($row->tid);
  $term = _lexicon_term_add_info($term);
  if (is_object($term)) {
	$n = new StdClass();

        $n->site = "General";                          // domain as it appears in the results
        $n->bundle = "Glossary";                       // subdomain
        $n->bundle_name = "Glossary";                  // subdomain as it appears in the results
        $n->label = taxonomy_term_title($term);        // Term name (displays as link in results)
        $n->entity_type = "Glossary";
        $n->id = $term->tid;
        $n->timestamp = date(EXPORT_DATE_FORMAT, $term->created);
        $n->content = $term->description;
        if ( $n->content ){
           $n->description = strip_tags($n->content);
        }

	$term_uri = taxonomy_term_uri($term); // get array with path
	$term_title = taxonomy_term_title($term);
	$term_path = $term_uri['path'];
	$link = check_plain(url($term_path, array()));
    	$n->url = parse_url($link, PHP_URL_PATH);
    	$n->path = parse_url($link, PHP_URL_PATH);
	$data->add->{$term->tid}['doc'] = $n;
    	$count++;
	
  }
}


// encode in json and snip the front/back to parse
$json = json_encode($data);
$json = jsonCleaner($json);
file_put_contents('/vectorbase/web/root/includes/search/indexes/drupal-indices/glossary-export.json', $json);
print "$count objects indexed.\n";
print "glossary-export.json generated in {DRUPAL_ROOT}/includes/search/indexes/drupal-indices folder\n";


// reformat the type to make it presentable for search results
function nameCleaner(string $name){
  $name = ucfirst($name);
  $name = str_replace("_", " ", $name);
  return $name;
}

// minor changes to the json file so that it parses correctly
function jsonCleaner(string $json){
  $json = substr($json, 7);
  $json = substr($json, 0, -1);
  // replace id with "doc"
  $pattern = '#"\d+":\{"doc":\{#';
  $replace = '"add":{"doc":{';
  return preg_replace($pattern, $replace, $json);
}

?>
